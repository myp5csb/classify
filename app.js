const express = require("express");
const mongoose = require("mongoose");
const keys = require("./config/keys");
const cookieSession = require("cookie-session");
const passport = require("passport");
// models
const bodyParser = require("body-parser");
require('./models/User');
// require('./models/Class');

mongoose.connect(
  keys.mongoURI,
  { useFindAndModify: false, useNewUrlParser: true, useUnifiedTopology: true },
  function(error) {
    if (error) console.error(error);
  }
);

// auth
require("./services/passport");

const app = express();

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);

app.use(passport.initialize());
app.use(passport.session());

// support parsing of application/json type post data
app.use(bodyParser.json({ limit: "50mb" }));
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

// oAuthroutes
require('./routes/auth')(app);
// require('./routes/classes')(app);

// if (process.env.NODE_ENV === 'production' ){
// Express serve production files
app.use(express.static("client/dist"));
//  Exrpess serve index.html file if doesn't recognize the file
const path = require("path");
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "client", "dist", "index.html"));
});

// }

const PORT = process.env.PORT || 5000;
app.listen(PORT);
