import Loading from '@/components/Base/Loading.vue';
import Card from '@/components/Cards/Card.vue';
import BaseButton from '@/components/BaseButton.vue';
/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install(Vue) {
    Vue.component(Card.name, Card);
    Vue.component(BaseButton.name, BaseButton);
    Vue.component(Loading.name, Loading);
  }
};

export default GlobalComponents;
