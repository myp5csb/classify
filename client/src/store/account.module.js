import axios from 'axios';
import router from '@/routes/router';

const RF_AUTH_ACCESSTOKEN = 'app_ezgg_auth'

let user = JSON.parse(localStorage.getItem(RF_AUTH_ACCESSTOKEN));
if (!user){
    user = false;
}
const state = {
        user,
        resettingPassword : '',
        loadingFavourite: false,
        changingPassword : ''
    };
const actions = {
	async login({dispatch,commit},{email,passwd}) {
		commit('loginRequest',email);
		try	{
			const { data } = await axios.post(`/api/login`,{email,passwd})
            commit('loginSuccess', data.user);
            localStorage.setItem(RF_AUTH_ACCESSTOKEN, JSON.stringify(data.token));
            
            const path = router.history.current.params.nextUrl ? router.history.current.params.nextUrl  : 'admin';
            console.log('goto ->',path);
            setTimeout(() => {router.push(path)},500);
		} catch(error){
            commit('loginFailure', error);
            dispatch('alert/error', error, { root: true });
		}
		
	},

    logout({ commit }) {
        localStorage.removeItem(RF_AUTH_ACCESSTOKEN);
        commit('logout');
    },

    async register({ dispatch, commit }, {email,passwd,name,surname}) {
        commit('registerRequest', {email,passwd,name,surname});
        try {
            const res = await axios.put(`/api/register`,{email,passwd,name,surname});
            commit('registerSuccess', res.data);

            router.push('/login');
            setTimeout(() => {
                // display success message after route change completes
                dispatch('alert/success', 'Registration successful', { root: true });
            });
        } catch(error){
            commit('registerFailure', error);
            setTimeout(() => {
                dispatch('alert/error', error, { root: true });
            });
        }
    },

    fetch({ commit }) {
      // Check if is logged in, otherwise return null 
      if ( localStorage.getItem(RF_AUTH_ACCESSTOKEN) ){
          const user = parseJwt(JSON.parse(localStorage.getItem(RF_AUTH_ACCESSTOKEN))) || null;
          commit('updateUser', user);
      }else{
          commit('updateUser', false);
      }
    },

    async addToFavourites({ commit,dispatch },id) {
        commit('addToFavouritesRequest');
        try{
            const res = await axios.get(`/api/favourites/presentations/${id}`)
            commit('addToFavouritesSuccess', res.data);
        } catch(e){
            commit('addToFavouritesFailure', e);
            dispatch('alert/error', e, { root: true });
        }
    },

    async resetPassword({ commit,dispatch }, email){
        commit('resetPasswdRequest');
        try{
            const res = await axios.get(`/api/users/reset-password/${email}`)
            commit('resetPasswdSuccess', res.data);
        } catch (e){
            commit('resetPasswdFailure', e);
            dispatch('alert/error', e, { root: true });            
        }
    },

    async changePassword({ commit,dispatch }, body){
        commit('changePasswdRequest');
        try{
            const res = await axios.put(`/api/users/change-password`,body)
            commit('changePasswdSuccess', res.data);
            setTimeout(() => {router.push('/login')},500);

        } catch (e){
            commit('changePasswdFailure', e);
            dispatch('alert/error', e, { root: true });            
        }
    }
}


const parseJwt =  (token) => {
    var base64Url = token.split('.')[1];
    var base64 = decodeURIComponent(atob(base64Url).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(base64);
};

const prepareUser = user => ( typeof user === 'object' ? {
        ...user,
        is: role => (user.roles.indexOf(role) >= 0)
    } : user);

const mutations =  {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = prepareUser(user);
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = prepareUser(user);
        console.log(state);
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    },
    registerRequest(state) {
        state.status = { registering: true };
    },
    registerSuccess(state) {
        state.status = {};
    },
    registerFailure(state) {
        state.status = {};
    },
    updateUser: (state, user) => {
        // console.log('updating user',user);
        state.user = prepareUser(user);
    },
    addToFavouritesRequest: (state) => {
        // console.log('updating user',user);
        state.loadingFavourite = true;
    },
    addToFavouritesSuccess: (state, user) => {
        // console.log('updating user',user);
        state.user = prepareUser(user);
        state.loadingFavourite = false;
    },
    addToFavouritesFailure: (state) => {
        // console.log('updating user',user);
        state.loadingFavourite = false;
    },
    resetPasswdRequest(state) {
        state.resettingPassword= 'requested';
    },
    resetPasswdSuccess(state) {
        state.resettingPassword= 'resetted';
    },
    resetPasswdFailure(state) {
        state.resettingPassword= 'errored';
    },
    changePasswdRequest(state) {
        state.changingPassword= 'requested';
    },
    changePasswdSuccess(state) {
        state.changingPassword= 'changed';
    },
    changePasswdFailure(state) {
        state.changingPassword= 'errored';
    }
  }


export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};