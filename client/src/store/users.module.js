import axios from 'axios';

const state = {
    users: [],
    loading: false, 
    user: null
};

const actions = {
    async getAll({ commit,dispatch }) {
        commit('getAllRequest');
        try{
            const res = await axios.get(`/api/users`)
            commit('getAllSuccess', res.data);
        } catch(e){
            commit('getAllFailure', e);
            dispatch('alert/error', e, { root: true });
        }
    },

    async update({ commit,dispatch }, user) {
        commit('updateRequest', user);
        try{
            const {data} = await axios.put(`/api/users/${user.id}`,{user})
            commit('updateSuccess', data);
            dispatch('alert/success', 'Utente modificato correttamente', { root: true });
        } catch(e){
            commit('updateFailure', e);
            dispatch('alert/error', e, { root: true });
        }
    },

    async delete({ commit,dispatch }, id) {
        commit('deleteRequest', id);
        try{
            const {data} = await axios.delete(`/api/users/${id}`)
            commit('deleteSuccess', data);
        } catch(e){
            commit('deleteFailure', e);
            dispatch('alert/error', e, { root: true });
            
        }
    },
};

const mutations = {
    getAllRequest(state) {
        state.loading = true;
        state.users = [];
    },
    getAllSuccess(state, users) {
        state.users = users;
    },
    getAllFailure(state, error) {
        state.error = error;
    },
    updateRequest(state, user) {
        state.user = {
            ...user,
            loading: true
        };
    },
    updateSuccess(state, newUser) {
        state.users = state.users.map(user => (user.id === newUser.id ? newUser : user))
        state.user = newUser;
    },
    updateFailure(state, error) {        
        state.user = { ...state.user, error };
    },
    deleteRequest(state, id) {
        // add 'deleting:true' property to user being deleted
        state.users = state.users.map(user =>
            user.id === id
                ? { ...user, deleting: true }
                : user
        )
    },
    deleteSuccess(state, id) {
        // remove deleted user from state
        state.users = state.users.filter(user => user.id !== id)
    },
    deleteFailure(state, { id, error }) {
        // remove 'deleting:true' property and add 'deleteError:[error]' property to user 
        state.users = state.users.map(user => {
            if (user.id === id) {
                // make copy of user without 'deleting:true' property
                const { ...userCopy } = user;
                // return copy of user with 'deleteError:[error]' property
                return { ...userCopy, deleteError: error };
            }

            return user;
        })
    },
};

export const users = {
    namespaced: true,
    state,
    actions,
    mutations
};