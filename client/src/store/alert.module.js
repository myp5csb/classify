const state = {
    type: null,
    message: null
};

const actions = {
    success({ commit }, message) {
        commit('success', message);
        setTimeout(() => {
            commit('clear',null);
        },2000)
    },
    error({ commit }, message) {
        commit('error', message);
        setTimeout(() => {
            commit('clear',null);
        },8000)
    },
    clear({ commit }) {
        commit('clear', null);
    }
};

const mutations = {
    success(state, message) {
        if (message){
            state.type = 'success';
            let finalMessage = message.data ? message.data.message : message
            state.message = finalMessage.toString();
        }
    },
    error(state, message) {
        state.type = 'danger';
        if (message !== undefined){
            let finalMessage = message;
            if (message.message !== undefined){
                finalMessage = message.message;
            }
            if (message.data !== undefined){
                finalMessage = `Internal Server Error: ${message.data.message || message.statusText}`;
            }
            state.message = finalMessage;
        }
    },
    clear(state) {
        state.type = null;
        state.message = null;
    }
};

export const alert = {
    namespaced: true,
    state,
    actions,
    mutations
};