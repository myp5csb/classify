import axios from 'axios';

//setup axios -> // TODO -> inseriscilo nell'applicativo per inserire un loading alle richieste
axios.interceptors.request.use(function (config) {
    // TODO -> Centralizing this variable
    const RF_AUTH_ACCESSTOKEN = 'app_lfr_auth'

    if (localStorage.getItem(RF_AUTH_ACCESSTOKEN)){
        config.headers = {
            ...config.headers,
            Authorization: `Bearer ${JSON.parse(localStorage.getItem(RF_AUTH_ACCESSTOKEN))}`,
        }
    }
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    // non uso router perché potrebber non essere ancora valorizzata la sua history
    const path = window.location.hash.substr(2,1000);
    if (error.response.status === 401 && path !== "login"){
        // //this doesn't work, i think because the route login isn't a ase router route
        debugger
        router.push('/login');
    }else{
        // Do something with response error
        // console.log(error,error.response,store);
        // store.dispatch('alert/error',error.statusText,{root:true});
        return Promise.reject(error.response);
        // throw new Error(error.response);        
    }
  });
