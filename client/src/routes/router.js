import VueRouter from 'vue-router';
import routes from './routes';
// import { sstore } from '@/store/store';

import axios from 'axios';

// configure router
const router = new VueRouter({
  mode: 'history',
  routes, // short for routes: routes
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
        return savedPosition
    } else {
        return { x: 0, y: 0 }
    }
  }
});

// router.beforeEach((to, from, next) => {
//   store.dispatch('account/fetch');
//   if(to.matched.some(record => record.meta.requiresAuth)) {
//         if (!store.state.account.user) {
//             const path = to !== undefined ? to.fullPath : null;
//             next({
//                 path: '/login',
//                 params: { 
//                     nextUrl: path,
//                 }
//             });
//         } else {
//             // to do, check roles
//             // let user = JSON.parse(localStorage.getItem('user'))
//             // if(to.matched.some(record => record.meta.is_admin)) {
//             //     if(user.is_admin == 1){
//             //         next()
//             //     }
//             //     else{
//             //         next({ name: 'userboard'})
//             //     }
//             // }else {
//             //     next()
//             // }
//             next();
//         }
//     }else {
//         next() ;
//     }
// });



// //setup axios -> // TODO -> inseriscilo nell'applicativo per inserire un loading alle richieste
// axios.interceptors.request.use(function (config) {
//     // TODO -> Centralizing this variable
//     const RF_AUTH_ACCESSTOKEN = 'app_lfr_auth'

//     if (localStorage.getItem(RF_AUTH_ACCESSTOKEN)){
//         config.headers = {
//             ...config.headers,
//             Authorization: `Bearer ${JSON.parse(localStorage.getItem(RF_AUTH_ACCESSTOKEN))}`,
//         }
//     }
//     return config;
//   }, function (error) {
//     // Do something with request error
//     return Promise.reject(error);
//   });

// // Add a response interceptor
// axios.interceptors.response.use(function (response) {
//     // Do something with response data
//     return response;
//   }, function (error) {
//     // non uso router perché potrebber non essere ancora valorizzata la sua history
//     const path = window.location.hash.substr(2,1000);
//     if (error.response.status === 401 && path !== "login"){
//         debugger
//         // //this doesn't work, i think because the route login isn't a base router route
//         router.push({
//             path: '/login', 
//             params: { 
//                 nextUrl: path
//             }
//         });
//     }else{
//         // Do something with response error
//         // console.log(error,error.response,store);
//         // store.dispatch('alert/error',error.statusText,{root:true});
//         return Promise.reject(error.response);
//         // throw new Error(error.response);        
//     }
//   });



export default router;
