import DashboardLayout from '@/views/dashboard/DashboardLayout.vue';
import AuthLayout from '@/views/auth/AuthLayout.vue';

// GeneralViews
import NotFound from '@/views/NotFound.vue';

// presentations ecc...
const Login = () =>
  import(/* webpackChunkName: "authpages" */ '@/views/auth/Login.vue');
const ChangePassword = () =>
  import(/* webpackChunkName: "authpages" */ '@/views/auth/ChangePassword.vue');
const ResetPassword = () =>
  import(/* webpackChunkName: "authpages" */ '@/views/auth/ResetPassword.vue');
const Register = () =>
  import(/* webpackChunkName: "authpages" */ '@/views/auth/Register.vue');


// Dashboard pages
const Dashboard = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/Dashboard.vue');

// Dashboard pages
const Eros = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/Eros.vue');

// Dashboard pages
const Pietro = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/Pietro.vue');

// Dashboard pages
const Candy = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/Candy.vue');

let authPages = {
  path: '/',
  component: AuthLayout,
  name: 'Authentication',
  children: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/reset-password',
      name: 'ResetPassword',
      component: ResetPassword
    },
    {
      path: '/change-password/:token',
      name: 'ChangePassword',
      component: ChangePassword
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    }
  ]
};

const routes = [
  {
    path: '/',
    redirect: '/admin',
    name: 'Home'
  },
  authPages,
  {
    path: '/admin',
    component: DashboardLayout,
    name: 'Dashboard',
    children: [
      {
        path: '',
        name: 'Dashboard',
        components: { default: Dashboard },    
        meta: { 
          requiresAuth: true
        }
      },

      {
        path: '/eros',
        name: 'Dashboard',
        components: { default: Eros },    
        meta: { 
          requiresAuth: true
        }
      },
      {
        path: '/pietro',
        name: 'Dashboard',
        components: { default: Pietro },    
        meta: { 
          requiresAuth: true
        }
      },
      {
        path: '/candy',
        name: 'Dashboard',
        components: { default: Candy },    
        meta: { 
          requiresAuth: true
        }
      },
    ]
  },
  { path: '*', component: NotFound }
];

export default routes;
