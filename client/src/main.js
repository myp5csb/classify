import Vue from 'vue'
import VueRouter from 'vue-router';

import VeeValidate from 'vee-validate';
import Antd from 'ant-design-vue';

import App from './App.vue'
import { store } from '@/store/store'
import router from '@/routes/router';
import GlobalComponents from '@/components/GlobalComponents'
// import './registerServiceWorker';

// import 'element-ui/lib/theme-chalk/reset.css'
import 'ant-design-vue/dist/antd.css';

import { library } from '@fortawesome/fontawesome-svg-core'

import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(fas)
library.add(fab)
library.add(far)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(GlobalComponents)
Vue.use(Antd);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
