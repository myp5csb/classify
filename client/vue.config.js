module.exports = {
  productionSourceMap: false,
  css: {
    // Enable CSS source maps.
    sourceMap: process.env.NODE_ENV !== 'production'
  },
  devServer: {
    proxy: {
      '/api/*': {
        target: 'http://localhost:5000',
        // ws: true,
        // changeOrigin: true => interesting
      },
      '/auth/google': {
        target: 'http://localhost:5000'
      },

    }
  }
};