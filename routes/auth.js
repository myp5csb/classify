var express = require("express");
// var router = express.Router();
var passport = require("passport");

// Middleware to check if the user is authenticated
function isUserAuthenticated(req, res, next) {
  if (req.user) {
    next();
    // console.log(req.user);
    
  } else {
    res.send("You must login!");
  }
}

module.exports = app => {
  // passport.authenticate middleware is used here to authenticate the request
  app.get(
    "/auth/google",
    passport.authenticate("google", {
      scope: ["profile"] // Used to specify the required data
    })
  );

  // The middleware receives the data from Google and runs the function on Strategy config
  app.get(
    "/auth/google/callback",
    passport.authenticate("google"),
    (req, res) => {
      console.log(req.user)
      res.redirect("/secret");
    }
  );

  // Secret route
  app.get("/secret", isUserAuthenticated, (req, res) => {
    res.send("You have reached the secret route");
  });

  // Logout route
  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });
};
