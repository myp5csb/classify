const mongoose = require('mongoose');

const { Schema } = mongoose

const UserSchema = new Schema({
    googleId: String,
    googleProfile: new Schema({
        name: String,
        given_name: String,
        family_name: String,
        picture: String,
        locale: String
    })
})

mongoose.model('User', UserSchema)