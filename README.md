# Classify

*__Note: Every command must be executed from terminal, after you navigate to the root folder of the project (the one where this file is located). To navigate trough folders just use the command "cd "__*
Open the terminal e go in the root folder:
```sh
cd folder-name/another-folder/classify
```
---
### Configure the project

Run this commands only the first time to configure your computer and let them be able to work.

```sh
npm i -g @vue/cli
npm i -g nodemon
npm i -g concurrently
npm i 
cd client 
npm i 
cd ..
```

The first 3 commands, will install two tool that we help us to *Run the code*:
- __vue-cli__, that is important to run the code of the front-end
- __nodemon__, that is important to run the code of the back-end
- __concurrently__, will be helpfull to run at the same time both the frontend and the backend

The other 4 commands does this: 
- Install the dependencies for the back-end (see "__Porject structure__ and __Get and update the dependencies__ sections to understand")
- Go inside the front-end folder
- Install the dependencies for the frontend-end
- Go back to the root folder of the project 

---
### Run the code

From the root folder run this command from terminal:

```sh
npm run dev
```

Then, wait for your browser (__You must work with Google chrome__) to open the tab of the project. You are ready to go. When you will save a file, the tab will refresh itself.

When you want to close the process, type __ctrl+C__ on the terminal and confirm to exit.

---
### Project structure

The most important thing to remember is the division between the front-end application and the back-end application.
- All the front-end application is inside the __client__ folder. 
- All the back-end application is outside of the __client__ folder.

*Regardless on whatever you are coding on, you must have both the application updated and working in order to see the results.*

---
### Get and update the dependencies

Both project (__front-end__ inside client application and __back-end__ outside), use different packages in order to work better, for example: 
- the front-end use __moment__ , a framework that will help us to display dates in a correct format.
- the back-end use __express__, a framework that will help us to build the APIs

To manage this dependencies, we list them in a file called __*packages.json*__ .
*__Both project has its own package.json file, because they are two different application__*
- The packages.json file for the front-end is located in __*client/package.json*__
- the packages.json file for the back-end is located in the root folder, __*/packages.json*__


While we are going to work, we could add new tools to our project, so sometimes you will need to add some node packages or get some node packages that other students added to the project. 

If you want to add a resource on the front-end application, you will need to:
- Stop the project if it's running
- go inside the client folder
- execute the command  npm i -s *name-of-your-package*
- go back to the root folder of the project 
- Re-run the project

```sh
## Stopped the project
cd client
npm i -s name-of-your-package
cd ..
npm run dev
```

###### Same goes for the back-end but you won't need to go inside the client folder.
---

### Working with GIT 

1. Every time you start to work, you need to __get__ the last version of the software that we are developing. 
2. Every time you finish to work, you need to __send__ your work to the others, so they will be update with the things you did.

### Get the code
To get the code, just run (from the root of your application):
```sh
git pull origin master
```
### Send the code
To get the code, there are three steps to follow:
1. __You need to tell git the things you edit run (from the root of your application):__
```sh
git add .
git commit -m"Message that describe what you did"
```
2. __You need, before sending to everyone your work, be sure that no-one made any edit since you started to work__
So you __get__ the code again:
```sh
git pull origin master
```
3. __You push the final code to your class__
```sh
git push origin master
```

### Git Conflict and how to solve them
After *__getting__* the code, you could face some problems.
Ask yourself this question:

*__What happen if some other student, edit the same file I edited and I get your edits inside my code?__*

GIT automatically merge the files into a new one, using the new line of your students and the new ones of yours.

*__BUT. What happen if two students edit the same file and the same row in that spacific file?__*
In this case, Git can't merge the files togheter because both students did an edit on the same row. This fill create a *__Conflict__*

Don't worry, this happens a lot of times and at the beggining will happen even often. 
You will see that you made a conflict because the output of the command "*git pull origin master*", so you need to read the output of every git command you make.

The output of a command is something like that:

```sh
Auto-merging origin_settings.py
CONFLICT (content): Merge conflict in nameofthefile.js
Auto-merging nameofthefile.js
Automatic merge failed; fix conflicts and then commit the result.
```

In this case follow this steps:
1. Call the other student that did the edit
2. Call the teacher and the architect of your deparment
3. Open gitkraken 
4. Merge the file manually thanks to git kraken, with the other students and the supervision of the teacher.


After you Fix, you can finally proceed to Send the file to the repository.
