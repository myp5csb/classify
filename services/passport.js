const passport = require('passport');
const GoogleStrategy = require("passport-google-oauth20");
const keys = require('../config/keys');
const mongoose = require('mongoose')
const User = mongoose.model('User')


passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientId,
      clientSecret: keys.googleClientSecret,
      callbackURL: "http://localhost:5000/auth/google/callback"
    },
    async (accessToken, refreshToken, profile, done) => {
      const userToCheck = await User.findOne({
        googleId: profile.id
      })

      if (! userToCheck) {
        //insert new user
        const newUser = await new User({
          googleId: profile.id,
          googleProfile: profile._json
        }).save()
        done(null, newUser); // passes the profile data to serializeUser
      }else{
        done(null, userToCheck)
      }
    }
  )
);

// Used to stuff a piece of information into a cookie
passport.serializeUser((user, done) => {
  done(null, user);
});

// Used to decode the received cookie and persist session
passport.deserializeUser((user, done) => {
  done(null, user);
});
